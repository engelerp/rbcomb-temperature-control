%TF.GenerationSoftware,KiCad,Pcbnew,(6.0.1)*%
%TF.CreationDate,2023-03-20T15:10:12+01:00*%
%TF.ProjectId,heating_driver,68656174-696e-4675-9f64-72697665722e,rev?*%
%TF.SameCoordinates,Original*%
%TF.FileFunction,Soldermask,Top*%
%TF.FilePolarity,Negative*%
%FSLAX46Y46*%
G04 Gerber Fmt 4.6, Leading zero omitted, Abs format (unit mm)*
G04 Created by KiCad (PCBNEW (6.0.1)) date 2023-03-20 15:10:12*
%MOMM*%
%LPD*%
G01*
G04 APERTURE LIST*
G04 Aperture macros list*
%AMRoundRect*
0 Rectangle with rounded corners*
0 $1 Rounding radius*
0 $2 $3 $4 $5 $6 $7 $8 $9 X,Y pos of 4 corners*
0 Add a 4 corners polygon primitive as box body*
4,1,4,$2,$3,$4,$5,$6,$7,$8,$9,$2,$3,0*
0 Add four circle primitives for the rounded corners*
1,1,$1+$1,$2,$3*
1,1,$1+$1,$4,$5*
1,1,$1+$1,$6,$7*
1,1,$1+$1,$8,$9*
0 Add four rect primitives between the rounded corners*
20,1,$1+$1,$2,$3,$4,$5,0*
20,1,$1+$1,$4,$5,$6,$7,0*
20,1,$1+$1,$6,$7,$8,$9,0*
20,1,$1+$1,$8,$9,$2,$3,0*%
G04 Aperture macros list end*
%ADD10R,2.000000X2.000000*%
%ADD11O,2.000000X2.000000*%
%ADD12R,1.905000X2.000000*%
%ADD13O,1.905000X2.000000*%
%ADD14R,1.700000X1.700000*%
%ADD15O,1.700000X1.700000*%
%ADD16R,8.000000X18.800000*%
%ADD17R,3.302000X1.498600*%
%ADD18RoundRect,1.000000X-1.000000X-3.000000X1.000000X-3.000000X1.000000X3.000000X-1.000000X3.000000X0*%
G04 APERTURE END LIST*
D10*
%TO.C,D1*%
X116455000Y-60460000D03*
D11*
X116455000Y-65540000D03*
%TD*%
D12*
%TO.C,M1*%
X121940000Y-72655000D03*
D13*
X119400000Y-72655000D03*
X116860000Y-72655000D03*
%TD*%
D14*
%TO.C,J3*%
X128000000Y-36000000D03*
D15*
X125460000Y-36000000D03*
%TD*%
D16*
%TO.C,L1*%
X108200000Y-73000000D03*
X89800000Y-73000000D03*
%TD*%
D17*
%TO.C,C1*%
X109702000Y-55200000D03*
X103098000Y-55200000D03*
%TD*%
D18*
%TO.C,J1*%
X90000000Y-39500000D03*
X81000000Y-39500000D03*
%TD*%
%TO.C,J2*%
X113500000Y-39500000D03*
X104500000Y-39500000D03*
%TD*%
M02*
