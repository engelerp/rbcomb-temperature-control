%TF.GenerationSoftware,KiCad,Pcbnew,(6.0.1)*%
%TF.CreationDate,2023-03-20T15:11:28+01:00*%
%TF.ProjectId,heating_board_2,68656174-696e-4675-9f62-6f6172645f32,rev?*%
%TF.SameCoordinates,Original*%
%TF.FileFunction,Soldermask,Top*%
%TF.FilePolarity,Negative*%
%FSLAX46Y46*%
G04 Gerber Fmt 4.6, Leading zero omitted, Abs format (unit mm)*
G04 Created by KiCad (PCBNEW (6.0.1)) date 2023-03-20 15:11:28*
%MOMM*%
%LPD*%
G01*
G04 APERTURE LIST*
G04 Aperture macros list*
%AMRoundRect*
0 Rectangle with rounded corners*
0 $1 Rounding radius*
0 $2 $3 $4 $5 $6 $7 $8 $9 X,Y pos of 4 corners*
0 Add a 4 corners polygon primitive as box body*
4,1,4,$2,$3,$4,$5,$6,$7,$8,$9,$2,$3,0*
0 Add four circle primitives for the rounded corners*
1,1,$1+$1,$2,$3*
1,1,$1+$1,$4,$5*
1,1,$1+$1,$6,$7*
1,1,$1+$1,$8,$9*
0 Add four rect primitives between the rounded corners*
20,1,$1+$1,$2,$3,$4,$5,0*
20,1,$1+$1,$4,$5,$6,$7,0*
20,1,$1+$1,$6,$7,$8,$9,0*
20,1,$1+$1,$8,$9,$2,$3,0*%
G04 Aperture macros list end*
%ADD10RoundRect,0.500000X1.580480X-2.000521X-0.689473X2.454512X-1.580480X2.000521X0.689473X-2.454512X0*%
%ADD11RoundRect,1.000000X-2.803315X-1.463361X-1.364635X-2.852678X2.803315X1.463361X1.364635X2.852678X0*%
G04 APERTURE END LIST*
D10*
%TO.C,F1*%
X104825119Y-169362418D03*
X116174881Y-191637581D03*
%TD*%
D11*
%TO.C,J1*%
X132368707Y-205025138D03*
X125894649Y-198773212D03*
%TD*%
M02*
