#include <SPI.h>
#include <ltc6992.hpp>
#include <ad7124_4.hpp>
#include <ad7124_registers.hpp>
#include <spi_selecta.hpp>
#include <temp_table.hpp>

#define SPIRATE 84000000/32

#define nCS_0 4
#define nCS_1 10
#define nCS_2 52
#define nCS_3 46

SPISettings spisettings = SPISettings(SPIRATE, MSBFIRST, SPI_MODE3);
Ad7124_4 adc_1(nCS_1, &spisettings);
Ad7124_4 adc_2(nCS_2, &spisettings);
double max_drive = 0.99;
double min_drive = 0.01;
uint32_t adc1_data;
uint32_t adc2_data;
double adc2_temps[4] = {0., 0., 0., 0.};

//uint32_t target_value = 0x0073FFFF;
double target_temperature = 34.0;

double pid_arr[3] = {0., 0., 0.}; //P I D
//double kpid_arr[3] = {10., 0.008, 8*50.}; //Kp Ki Kd
//double kpid_arr[3] = {2., 0.008, 4.*50.}; //Kp Ki Kd
double kpid_arr[3] = {2., 0.0, 0.}; //Kp Ki Kd
double pid_output = 0.;

/*
error is to be positive when current_temperature < target_temperature
pid = {val_p, val_i, val_d}
kpid = {Kp, Ki, Kd}
output will be in [0.01, 0.99]
*/
void update_pid(double error, double* pid, const double* kpid, double& output){
  double pid_offset = 0.653;
  pid[2] = error - pid[0];
  (error*error < 0.15) ? pid[1] += error : pid[1] = 0.;
  pid[0] = error;
  output = pid_offset + kpid[0]*pid[0] + kpid[1]*pid[1] + kpid[2]*pid[2];
  //output = kpid[0]*pid[0] + kpid[1]*pid[1] + kpid[2]*pid[2];
  /*clamp to [0, 1]*/
  double maxval = 0.99;
  double minval = 0.01;
  if(output > maxval){
    output = maxval;
  }
  if(output < minval){
    output = minval;
  }  
}

/*Timekeeping*/
unsigned long t0;
unsigned long t1;

/*Printing Buffer*/
char printBuf[256];


void setup() {
  Serial.begin(9600);
  //Serial.begin(460800);
  //Serial.begin(128000);
  Ltc6992::init();

  Spi_selecta::register_device(&adc_1);
  Spi_selecta::register_device(&adc_2);

  SPI.begin();
  delay(2000);


  Spi_selecta::select_device(&adc_1);
      adc_1.reset();
      delay(2000);
      adc_1.write_register<uint16_t>(
        REG_ADC_CONTROL,
        ADC_CONTROL_REF_EN_1 | ADC_CONTROL_POWER_MODE_FULL_POWER | ADC_CONTROL_Mode_CONTINUOUS | ADC_CONTROL_CLK_SEL_INTERNAL | ADC_CONTROL_nCS_EN_1
      );
      adc_1.write_register<uint16_t>(
        REG_CONFIG_0,
        CONFIG_Burnout_OFF | CONFIG_REF_BUFP_1 | CONFIG_REF_BUFM_1 | CONFIG_AIN_BUFP_1 | CONFIG_AIN_BUFM_1 | CONFIG_REF_SEL_INTERNAL | CONFIG_PGA_GAIN1
      );
      adc_1.write_register<uint32_t>(
        REG_FILTER_0,
        FILTER_Filter_SINC4 | FILTER_FS_2047
      );
      adc_1.write_register<uint16_t>(
        REG_CHANNEL_0,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN0 | CHANNEL_AINM_AIN1
      );
  Spi_selecta::deselect_device();

  Spi_selecta::select_device(&adc_2);
      adc_2.reset();
      delay(2000);
      adc_2.write_register<uint16_t>(
        REG_ADC_CONTROL,
        ADC_CONTROL_REF_EN_1 | ADC_CONTROL_POWER_MODE_FULL_POWER | ADC_CONTROL_Mode_CONTINUOUS | ADC_CONTROL_CLK_SEL_INTERNAL | ADC_CONTROL_nCS_EN_1// | ADC_CONTROL_DATA_STATUS_1
      );
      adc_2.write_register<uint16_t>(
        REG_CONFIG_0,
        CONFIG_Burnout_OFF | CONFIG_REF_BUFP_1 | CONFIG_REF_BUFM_1 | CONFIG_AIN_BUFP_1 | CONFIG_AIN_BUFM_1 | CONFIG_REF_SEL_INTERNAL | CONFIG_PGA_GAIN1
      );
      adc_2.write_register<uint32_t>(
        REG_FILTER_0,
        FILTER_Filter_SINC4 | FILTER_FS_2047
      );
      /*Setup channel 0*/
      adc_2.write_register<uint16_t>(
        REG_CHANNEL_0,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN0 | CHANNEL_AINM_AIN1
      );
      /*Setup channel 1*/
      adc_2.write_register<uint16_t>(
        REG_CHANNEL_1,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN2 | CHANNEL_AINM_AIN3
      );
      /*Setup channel 2*/
      adc_2.write_register<uint16_t>(
        REG_CHANNEL_2,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN4 | CHANNEL_AINM_AIN5
      );
      //Setup channel 3
      /*
      adc_2.write_register<uint16_t>(
        REG_CHANNEL_3,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN6 | CHANNEL_AINM_AIN7
      );
      */
  Spi_selecta::deselect_device();
  
  t0 = micros();
  t1 = micros();
}

void loop() {



  Spi_selecta::select_device(&adc_1);
  if(adc_1.data_available()){
    Spi_selecta::deselect_device();
    //Fetch Data
    Spi_selecta::select_device(&adc_1);
    adc1_data = adc_1.get_data();
    Spi_selecta::deselect_device();
    adc1_data &= 0x00FFFFFF;
    //Calculate Temperature
    double current_temperature = data_to_temperature(adc1_data);
  }
  Spi_selecta::deselect_device();

  Spi_selecta::select_device(&adc_2);
  if(adc_2.data_available()){
    Spi_selecta::deselect_device();
    Spi_selecta::select_device(&adc_2);
    uint32_t status_channel1 = adc_2.read_register(REG_Status, 1);
    Spi_selecta::deselect_device();
    Spi_selecta::select_device(&adc_2);
    adc2_data = adc_2.get_data();
    Spi_selecta::deselect_device();
    Spi_selecta::select_device(&adc_2);
    //uint32_t error = (adc_2.read_register(REG_Error, 3) & 0xFFFFFF);
    Spi_selecta::deselect_device();
    //uint32_t adc2_data_backup = adc2_data;
    adc2_data &= 0x00FFFFFF;
    if(status_channel1 < 4){
      adc2_temps[status_channel1] = data_to_temperature(adc2_data);
    }
    /*
    Serial.print("Valid Channel: ");
    Serial.print(status_channel1, HEX);
    Serial.print(", Data: 0x");
    Serial.print(adc2_data_backup, HEX);
    Serial.print(", Error: ");
    Serial.print(error, HEX);
    Serial.print("\n");
    */
  }
  Spi_selecta::deselect_device();
  

  if(micros() > t1 + 750000){ //print every 750ms
    //Format: T_loop T_ool_1 T_ool_2 T_ool_3 PID
    sprintf(printBuf, "Tloop:%f,Tool1:%f,Tool2:%f,Tool3:%f,Tool4:%f,PID:%f\n", data_to_temperature(adc1_data), adc2_temps[0], adc2_temps[1], adc2_temps[2], adc2_temps[3], pid_output);
    Serial.print(printBuf);
    t1 = micros();
  }


  //Update PID
  if(Serial.available() >= 2){
    char byte1_rd = Serial.read();
    char byte2_rd = Serial.read();
    uint32_t num_rd = uint32_t(byte1_rd) | (uint32_t(byte2_rd) << 8);
    double double_rd = static_cast<double>(num_rd) / static_cast<double>(0x1000);
    Serial.print("Received: 0x");
    Serial.print(num_rd, HEX);
    Serial.print(" (");
    Serial.print(double_rd, 10);
    Serial.print(")\n");
    if(double_rd < 1. && double_rd >= 0.){
      Ltc6992::set_level(double_rd);
      pid_output = double_rd;
    }
  }
  /*
  if(micros() > t0 + 200000){ //This only runs every 200ms 
    pid_output = 0.;
    update_pid(target_temperature - current_temperature, pid_arr, kpid_arr, pid_output);
    //Update PWM Output
    if(pid_output*pid_output <= 1.){
      Ltc6992::set_level(pid_output);
      //Ltc6992::set_level(0.42);
    }
    t0 = micros();
  }
  */
}
