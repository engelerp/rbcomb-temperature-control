#include <ad7124_4.hpp>
#include <ad7124_registers.hpp>
#include <spi_selecta.hpp>
#include <vector>

#include <SPI.h>

#define SPIRATE 84000000/32
//#define SPIRATE 84000000/128

#define nCS_0 4
#define nCS_1 10
#define nCS_2 52
#define nCS_3 46

SPISettings spisettings = SPISettings(SPIRATE, MSBFIRST, SPI_MODE3);

Ad7124_4 adc_3(nCS_3, &spisettings);
Ad7124_4 adc_0(nCS_0, &spisettings);


void setup() {
  Serial.begin(128000);
  //Serial.begin(460800);
  //Serial.begin(128000);
  analogWriteResolution(12);
  analogWrite(DAC1, 0b000000000000);

  Spi_selecta::register_device(&adc_3);
  Spi_selecta::register_device(&adc_0);
  
  SPI.begin();
  delay(2000);

  //Setup ADC on channel 3
  Spi_selecta::select_device(&adc_3);
  adc_3.reset();
  delay(2000);
  adc_3.write_register<uint16_t>(
    REG_ADC_CONTROL, 
    ADC_CONTROL_REF_EN_1 | ADC_CONTROL_POWER_MODE_FULL_POWER | ADC_CONTROL_Mode_CONTINUOUS | ADC_CONTROL_CLK_SEL_INTERNAL_PIN
    );
  adc_3.write_register<uint16_t>(
    REG_CONFIG_0, 
    CONFIG_Burnout_OFF | CONFIG_REF_BUFP_1 | CONFIG_REF_BUFM_1 | CONFIG_AIN_BUFP_1 | CONFIG_AIN_BUFM_1 | CONFIG_REF_SEL_INTERNAL | CONFIG_PGA_GAIN1
    );
  adc_3.write_register<uint32_t>(
    REG_FILTER_0,
    FILTER_Filter_SINC4 | FILTER_FS_2047
  );
  adc_3.write_register<uint16_t>(
    REG_CHANNEL_0,
    CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN0 | CHANNEL_AINM_AIN1
  );
  Spi_selecta::deselect_device();

  //Setup ADC on channel 0
  Spi_selecta::select_device(&adc_0);
  adc_0.reset();
  delay(2000);
  adc_0.write_register<uint16_t>(
    REG_ADC_CONTROL, 
    ADC_CONTROL_REF_EN_1 | ADC_CONTROL_POWER_MODE_FULL_POWER | ADC_CONTROL_Mode_CONTINUOUS | ADC_CONTROL_CLK_SEL_INTERNAL_PIN
    );
  adc_0.write_register<uint16_t>(
    REG_CONFIG_0, 
    CONFIG_Burnout_OFF | CONFIG_REF_BUFP_1 | CONFIG_REF_BUFM_1 | CONFIG_AIN_BUFP_1 | CONFIG_AIN_BUFM_1 | CONFIG_REF_SEL_INTERNAL | CONFIG_PGA_GAIN1
    );
  adc_0.write_register<uint32_t>(
    REG_FILTER_0,
    FILTER_Filter_SINC4 | FILTER_FS_2047
  );
  adc_0.write_register<uint16_t>(
    REG_CHANNEL_0,
    CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN0 | CHANNEL_AINM_AIN1
  );
  Spi_selecta::deselect_device();
}

void loop() {

  uint32_t adc_data;
  Spi_selecta::select_device(&adc_0);
  if(adc_0.data_available()){
    adc_data = adc_0.get_data();
    uint8_t buf[7];
    buf[0] = uint8_t(adc_data & 0x000000FF);
    buf[1] = uint8_t((adc_data>>8) & 0x000000FF);
    buf[2] = uint8_t((adc_data>>16) & 0x000000FF);
    buf[3] = uint8_t(0x01);
    buf[4] = uint8_t(0x0a);
    buf[5] = uint8_t(0x0a);
    buf[6] = uint8_t(0x0a);
    Serial.write(buf, 7);
    Serial.flush();
    delay(10);
  }
  Spi_selecta::deselect_device();

  Spi_selecta::select_device(&adc_3);
  if(adc_3.data_available()){
    adc_data = adc_3.get_data();
    uint8_t buf[7];
    buf[0] = uint8_t(adc_data & 0x000000FF);
    buf[1] = uint8_t((adc_data>>8) & 0x000000FF);
    buf[2] = uint8_t((adc_data>>16) & 0x000000FF);
    buf[3] = uint8_t(0x02);
    buf[4] = uint8_t(0x0a);
    buf[5] = uint8_t(0x0a);
    buf[6] = uint8_t(0x0a);
    Serial.write(buf, 7);
    Serial.flush();
    delay(10);
  }
  Spi_selecta::deselect_device();
}
