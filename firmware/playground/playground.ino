#include <SPI.h>
#include <algorithm> //std::min

#define nCS 46




#define REG_READ 0b01000000
#define REG_WRITE 0b00000000

#define REG_STATUS 0x00
#define REG_ID 0x05
#define REG_ADC_CONTROL 0x01 
#define REG_DATA 0x02
#define REG_CHANNEL_0 0x09
#define REG_CONFIG_0 0x19
#define REG_FILTER_0 0x21

//REG_STATUS BITS
//REG_ADC_CONTROL BITS
#define POWER_MODE_FULLPOWER 0b0000000010000000
#define CLK_SEL_INTERNALANDPIN 0b0000000000000001
#define REF_EN 0b0000000100000000
//REG_DATA BITS
//REG_CHANNEL_0 BITS
#define CHANNEL_SETTING 0b1000000000000001
//REG_CONFIG_0 BITS
#define CONFIG_SETTING 0b0000000111110000
//REG_FILTER_0 BITS
#define FILTER_SETTING 0b000000000000011111111111


void reset_comm(){
  SPI.beginTransaction(SPISettings(84000000/128, MSBFIRST, SPI_MODE3));
  for(unsigned i = 0; i < 8; ++i){
    SPI.transfer(0xFF);
  }
  SPI.endTransaction();
}

//T must be either uint8_t, uint16_t or uint32_t
template <class T>
void SPIwrite(T data, uint8_t reg){
  uint8_t wrbuf[] = {0, 0, 0, 0};
  //set up write command
  wrbuf[0] = reg | REG_WRITE;
  uint8_t num_bytes = std::min(sizeof(data), 3u);
  for(unsigned i = 0; i < num_bytes; ++i){
    wrbuf[num_bytes-i] = data & 0xFF;
    data >>= 8;
  }
  SPI.beginTransaction(SPISettings(84000000/128, MSBFIRST, SPI_MODE3));
  SPI.transfer(wrbuf, num_bytes+1);
  SPI.endTransaction();
}

//T must be either uint8_t, uint16_t or uint32_t
template <class T>
void SPIread(T& data, uint8_t reg){
  uint8_t rdbuf[] = {0, 0, 0, 0};
  //set up write command
  rdbuf[0] = reg | REG_READ;
  uint8_t num_bytes = std::min(sizeof(data), 3u);
  SPI.beginTransaction(SPISettings(84000000/128, MSBFIRST, SPI_MODE3));
  SPI.transfer(rdbuf, num_bytes+1);
  SPI.endTransaction();
  for(unsigned i = 1; i <= num_bytes; ++i){
    data <<= 8;
    data += rdbuf[i];
  }
}

void setup_adc(){
  //ADC_CONTROL
  uint16_t data16 = REF_EN | POWER_MODE_FULLPOWER;
  SPIwrite(data16, REG_ADC_CONTROL);
  //CONFIG
  data16 = CONFIG_SETTING;
  SPIwrite(data16, REG_CONFIG_0);
  //FILTER
  data16 = FILTER_SETTING;
  SPIwrite(data16, REG_FILTER_0);
  //CHANNEL
  data16 = CHANNEL_SETTING;
  SPIwrite(data16, REG_CHANNEL_0);
}

bool data_ready(){
  uint8_t status;
  SPIread(status, REG_STATUS);
  return !(status>>8);
}

uint32_t get_data(){
  uint32_t data;
  SPIread(data, REG_DATA);
  return data;
}

double ntc_r[] = {46.67, 44.60, 42.64, 40.77, 38.99, 37.30, 35.70, 34.17, 31.32, 30.00, 28.74, 27.54, 26.40, 25.31, 24.27, 23.28, 22.33, 21.43, 20.57, 19.74, 18.96, 18.21, 17.49, 16.80, 16.15};
double ntc_c[] = {15., 16., 17., 18., 19., 20., 21., 22., 23., 24., 25., 26., 27., 28., 29., 30., 31., 32., 33., 34., 35., 36., 37., 38., 39.};

double temperature(double R){
  for(size_t i = 1; i < 25; ++i){
    if(ntc_r[i] < R){
      double frac = (ntc_r[i-1] - R) / (ntc_r[i-1] - ntc_r[i]);
      double temp = ntc_c[i-1] + frac;
      return temp;
    }
  }
  return -1.;
}

void setup() {
  Serial.begin(9600);
  digitalWrite(nCS, HIGH);
  pinMode(nCS, OUTPUT);
  SPI.begin();

  delay(1000);

  digitalWrite(nCS, LOW);
  setup_adc();
  digitalWrite(nCS, HIGH);

}

void loop() {
  digitalWrite(nCS, LOW);
  /*uint8_t id;
  SPIread(id, REG_ID);
  digitalWrite(nCS, HIGH);
  Serial.print(id, BIN);
  Serial.print("\n");*/
  uint32_t adc_data;
  if(data_ready()){
    adc_data = get_data();
    uint8_t buf[3];
    buf[0] = uint8_t(adc_data & 0x000000FF);
    buf[1] = uint8_t((adc_data>>8) & 0x000000FF);
    buf[2] = uint8_t((adc_data>>16) & 0x000000FF);
    Serial.write(buf, 3);
    Serial.flush();
    /*
    Serial.print("Data: \t");
    Serial.print(adc_data, HEX);
    Serial.print('\t');
    Serial.print(adc_data, BIN);
    Serial.print('\t');
    Serial.print(adc_data, DEC);
    Serial.print('\n');
    Serial.print("Voltage: \t");
    double voltage = 2.5 * static_cast<double>(adc_data) / static_cast<double>(uint32_t(0xFFFFFF));
    Serial.print(voltage, 8);
    Serial.print(" V\n");
    Serial.print("Resistance: \t");
    double Rth = 20 * voltage / (2.5 - voltage);
    Serial.print(Rth, 8);
    Serial.print(" kOhm\n");
    Serial.print("Temperature: ");
    Serial.print(temperature(Rth),6);
    Serial.print(" C\n\n\n");
    Serial.flush();
    delay(100);
    */
  }
  digitalWrite(nCS, HIGH);
}
