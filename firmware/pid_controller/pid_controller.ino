#include <SPI.h>
#include <ltc6992.hpp>
#include <ad7124_4.hpp>
#include <ad7124_registers.hpp>
#include <spi_selecta.hpp>
#include <temp_table.hpp>
#include <DueTimer.h>
#include <utility.hpp>
#include <pid_controller.hpp>
#include <communicator.hpp>




#define SPIRATE 84000000/32

#define nCS_0 4
#define nCS_1 10
#define nCS_2 52
#define nCS_3 46

/*SPI Settings*/
SPISettings spisettings = SPISettings(SPIRATE, MSBFIRST, SPI_MODE3);
/*ADC Device Handles*/
Ad7124_4 adc_1(nCS_1, &spisettings);
Ad7124_4 adc_2(nCS_2, &spisettings);
/*ADC Data Storage*/
Adc_data adc1Data;
Adc_data adc2Data;


/*Set up Timer Interrupt Infrastructure*/
volatile bool pidUpdRq = false;
const long pidUpdRqPeriod = 750000; //update pid every 750ms

void handlerPid(){
  pidUpdRq = true; //set pid update request flag
}

void setup() {
    SerialUSB.begin(9600);

    Ltc6992::init();

    Spi_selecta::register_device(&adc_1);
    Spi_selecta::register_device(&adc_2);

    SPI.begin();
    delay(2000);


    Spi_selecta::select_device(&adc_1);
      adc_1.reset();
      delay(2000);
      adc_1.write_register<uint16_t>(
        REG_ADC_CONTROL,
        ADC_CONTROL_REF_EN_1 | ADC_CONTROL_POWER_MODE_FULL_POWER | ADC_CONTROL_Mode_CONTINUOUS | ADC_CONTROL_CLK_SEL_INTERNAL | ADC_CONTROL_nCS_EN_1
      );
      adc_1.write_register<uint16_t>(
        REG_CONFIG_0,
        CONFIG_Burnout_OFF | CONFIG_REF_BUFP_1 | CONFIG_REF_BUFM_1 | CONFIG_AIN_BUFP_1 | CONFIG_AIN_BUFM_1 | CONFIG_REF_SEL_INTERNAL | CONFIG_PGA_GAIN1
      );
      adc_1.write_register<uint32_t>(
        REG_FILTER_0,
        FILTER_Filter_SINC4 | FILTER_FS_2047
      );
      adc_1.write_register<uint16_t>(
        REG_CHANNEL_0,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN0 | CHANNEL_AINM_AIN1
      );
    Spi_selecta::deselect_device();

    Spi_selecta::select_device(&adc_2);
      adc_2.reset();
      delay(2000);
      adc_2.write_register<uint16_t>(
        REG_ADC_CONTROL,
        ADC_CONTROL_REF_EN_1 | ADC_CONTROL_POWER_MODE_FULL_POWER | ADC_CONTROL_Mode_CONTINUOUS | ADC_CONTROL_CLK_SEL_INTERNAL | ADC_CONTROL_nCS_EN_1// | ADC_CONTROL_DATA_STATUS_1
      );
      adc_2.write_register<uint16_t>(
        REG_CONFIG_0,
        CONFIG_Burnout_OFF | CONFIG_REF_BUFP_1 | CONFIG_REF_BUFM_1 | CONFIG_AIN_BUFP_1 | CONFIG_AIN_BUFM_1 | CONFIG_REF_SEL_INTERNAL | CONFIG_PGA_GAIN1
      );
      adc_2.write_register<uint32_t>(
        REG_FILTER_0,
        FILTER_Filter_SINC4 | FILTER_FS_2047
      );
      /*Setup channel 0*/
      adc_2.write_register<uint16_t>(
        REG_CHANNEL_0,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN0 | CHANNEL_AINM_AIN1
      );
      /*Setup channel 1*/
      adc_2.write_register<uint16_t>(
        REG_CHANNEL_1,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN2 | CHANNEL_AINM_AIN3
      );
      /*Setup channel 2*/
      adc_2.write_register<uint16_t>(
        REG_CHANNEL_2,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN4 | CHANNEL_AINM_AIN5
      );
      //Setup channel 3
      
      adc_2.write_register<uint16_t>(
        REG_CHANNEL_3,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN6 | CHANNEL_AINM_AIN7
      );
      
    Spi_selecta::deselect_device();
  
    /*Start Interrupt Timers*/
    Timer.getAvailable().attachInterrupt(handlerPid).start(pidUpdRqPeriod);
}

void loop() {
    /*Read ADC Data*/
    //ADC 1
    adc1Data = readAdc(&adc_1);
    if(adc1Data.valid && pidUpdRq){
        pidUpdRq = false;
        Temperatures::temperatures[0] = data_to_temperature(adc1Data.data);
        Pid_controller::update_pid(Temperatures::temperatures[0]);
    }
    //ADC 2
    adc2Data = readAdc(&adc_2);
    if(adc2Data.valid && adc2Data.channel < 4u){
        Temperatures::temperatures[adc2Data.channel+1] = data_to_temperature(adc2Data.data);
    }

    /*Receive Commands*/
    Communicator::communicate();
}
