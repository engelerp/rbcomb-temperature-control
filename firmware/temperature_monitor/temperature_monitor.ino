#include <Arduino.h>
#include <SPI.h>
#include <ltc6992.hpp>
#include <ad7124_4.hpp>
#include <ad7124_registers.hpp>
#include <spi_selecta.hpp>
#include <temp_table.hpp>
#include <utility.hpp>
#include <temperature_communicator.hpp>




#define SPIRATE 84000000/32

#define nCS_0 4
#define nCS_1 10
#define nCS_2 52
#define nCS_3 46

/*SPI Settings*/
SPISettings spisettings = SPISettings(SPIRATE, MSBFIRST, SPI_MODE3);
/*ADC Device Handles*/
Ad7124_4 adc(nCS_2, &spisettings);
/*ADC Data Storage*/
Adc_data adcData;

void setup() {
    SerialUSB.begin(9600);

    Ltc6992::init();

    Spi_selecta::register_device(&adc);

    SPI.begin();
    delay(2000);

    Spi_selecta::select_device(&adc);
      adc.reset();
      delay(2000);
      adc.write_register<uint16_t>(
        REG_ADC_CONTROL,
        ADC_CONTROL_REF_EN_1 | ADC_CONTROL_POWER_MODE_FULL_POWER | ADC_CONTROL_Mode_CONTINUOUS | ADC_CONTROL_CLK_SEL_INTERNAL | ADC_CONTROL_nCS_EN_1// | ADC_CONTROL_DATA_STATUS_1
      );
      adc.write_register<uint16_t>(
        REG_CONFIG_0,
        CONFIG_Burnout_OFF | CONFIG_REF_BUFP_1 | CONFIG_REF_BUFM_1 | CONFIG_AIN_BUFP_1 | CONFIG_AIN_BUFM_1 | CONFIG_REF_SEL_INTERNAL | CONFIG_PGA_GAIN1
      );
      adc.write_register<uint32_t>(
        REG_FILTER_0,
        FILTER_Filter_SINC4 | FILTER_FS_2047
      );
      /*Setup channel 0*/
      adc.write_register<uint16_t>(
        REG_CHANNEL_0,
        CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN0 | CHANNEL_AINM_AIN1
      );
      /*Setup channel 1*/
      //adc.write_register<uint16_t>(
      //  REG_CHANNEL_1,
      //  CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN2 | CHANNEL_AINM_AIN3
      //);
      /*Setup channel 2*/
      //adc.write_register<uint16_t>(
      //  REG_CHANNEL_2,
      //  CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN4 | CHANNEL_AINM_AIN5
      //);
      //Setup channel 3
      //adc.write_register<uint16_t>(
      //  REG_CHANNEL_3,
      //  CHANNEL_Enable_1 | CHANNEL_Setup_0 | CHANNEL_AINP_AIN6 | CHANNEL_AINM_AIN7
      //);
      
    Spi_selecta::deselect_device();
}

void loop() {
    /*Read ADC Data*/
    adcData = readAdc(&adc);
    if(adcData.valid && adcData.channel < 2u){
        Temperatures::temperatures[adcData.channel] = data_to_temperature(adcData.data);
    }

    /*Receive Commands*/
    TemperatureCommunicator::communicate();
}
