#include <SPI.h>

#define nCS 46
#define REG_ID 0x05
#define REG_READ 0b01000000

void setup() {
  Serial.begin(9600);
  digitalWrite(nCS, HIGH);
  pinMode(nCS, OUTPUT);
  SPI.begin();
}

void loop() {
  SPI.beginTransaction(SPISettings(84000000/128, MSBFIRST, SPI_MODE3));
  digitalWrite(nCS, LOW);
  SPI.transfer(REG_ID | REG_READ);
  uint8_t id = SPI.transfer(0x00);
  digitalWrite(nCS, HIGH);
  SPI.endTransaction();
  Serial.print(id, HEX);
  delay(1000);
}
