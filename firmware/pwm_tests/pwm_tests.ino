#include <ltc6992.hpp>
#include <vector>

std::vector<double> levels(4096,0.);
size_t ind = 0;

void setup() {
  Serial.begin(9600);
  //Serial.begin(460800);
  //Serial.begin(128000);
  Ltc6992::init();

  double delta = 1. / 4096;
  for(size_t i = 0; i < levels.size(); ++i){
    levels[i] = i * delta;
  }
  
}

void loop() {
  if(++ind >= levels.size()){
    ind = 0;
  }
  Ltc6992::set_level(levels[ind]);

  delay(10);
}
