#ifndef PID_CONTROLLER_HPP_INCLUDED
#define PID_CONTROLLER_HPP_INCLUDED
#include <ltc6992.hpp>
#include <vector>

class Pid_controller{
public:
    static void init(double setpoint, std::vector<double> kpid, double I_reset, double I_reset_errsq);
    static void update_pid(double temperature);

    static void set_Kp(double Kp);
    static void set_Ki(double Ki);
    static void set_Kd(double Kd);
    static void set_I_reset(double I_reset);
    static void set_I_reset_errsq(double I_reset_errsq);
    static void set_setpoint(double temperature);

    static std::vector<double> get_kpid();
    static std::vector<double> get_pid();
    static double get_output();
    static double get_I_reset();
    static double get_setpoint();

    static void turn_off();
    static void turn_on();

private:
    static double setpoint_; //target temperature in °C
    static double output_; //output to LTC6992
    static std::vector<double> pid_vec_; //current values of P I D
    static std::vector<double> kpid_vec_; //coefficients Kp Ki Kd
    static double I_reset_; //reset value of I
    static double I_reset_errsq_; //value of error*error above which the I-term is held in reset
    
    static bool enabled_;
};

#endif
