#include <pid_controller.hpp>
#include <algorithm>
#include <ltc6992.hpp>

/*Arduino isn't C++ standard compliant, need to fix this myself*/
namespace std {
  void __throw_bad_alloc()
  {
    SerialUSB.println("Unable to allocate memory");
  }

  void __throw_length_error( char const*e )
  {
    SerialUSB.print("Length Error :");
    SerialUSB.println(e);
  }
}

//define statics
double Pid_controller::setpoint_=0.; //target temperature in °C
double Pid_controller::output_=0.05; //output to LTC6992
std::vector<double> Pid_controller::pid_vec_ = {0., 0., 0.}; //current values of P I D
std::vector<double> Pid_controller::kpid_vec_ = {0., 0., 0.}; //coefficients Kp Ki Kd
double Pid_controller::I_reset_ = 0.; //reset value of I
double Pid_controller::I_reset_errsq_ = 0.5; //value of error*error above which the I-term is held in reset
bool Pid_controller::enabled_ = true;


void Pid_controller::init(double setpoint, std::vector<double> kpid, double I_reset, double I_reset_errsq){
    setpoint_ = setpoint;
    kpid_vec_ = kpid;
    I_reset_ = I_reset;
    I_reset_errsq_ = I_reset_errsq;
}

void Pid_controller::update_pid(double temperature){
    if(!enabled_){ //if we have been turned off, stay off
        output_ = 0.09;
        Ltc6992::set_level(output_);
        return;
    }
    //calculate error from setpoint
    double error = setpoint_ - temperature;
    //update P,I,D
    pid_vec_[2] = error - pid_vec_[0];
    (error*error < I_reset_errsq_) ? pid_vec_[1] += error : pid_vec_[1] = I_reset_;
    pid_vec_[0] = error;
    //calculate output
    output_ = kpid_vec_[0]*pid_vec_[0] + kpid_vec_[1]*pid_vec_[1] + kpid_vec_[2]*pid_vec_[2];
    //clamp output to rails
    output_ = std::max(output_, 0.1);
    output_ = std::min(output_, 0.9);
    //send value to controller
    Ltc6992::set_level(output_);
}

void Pid_controller::set_Kp(double Kp){
    kpid_vec_[0] = Kp;
}

void Pid_controller::set_Ki(double Ki){
    kpid_vec_[1] = Ki;
}

void Pid_controller::set_Kd(double Kd){
    kpid_vec_[2] = Kd;
}

void Pid_controller::set_I_reset(double I_reset){
    I_reset_ = I_reset;
    pid_vec_[1] = I_reset;
}

void Pid_controller::set_I_reset_errsq(double I_reset_errsq){
    I_reset_errsq_ = I_reset_errsq;
}

void Pid_controller::set_setpoint(double temperature){
    setpoint_ = temperature;
}

std::vector<double> Pid_controller::get_kpid(){
    return kpid_vec_;
}

std::vector<double> Pid_controller::get_pid(){
    return pid_vec_;
}

double Pid_controller::get_output(){
    return output_;
}

double Pid_controller::get_I_reset(){
    return I_reset_;
}

double Pid_controller::get_setpoint(){
    return setpoint_;
}

void Pid_controller::turn_off(){
    output_ = 0.09;
    enabled_ = false;
}

void Pid_controller::turn_on(){
    enabled_ = true;
}