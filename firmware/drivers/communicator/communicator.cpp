#include <communicator.hpp>
#include <cstdio> //snprintf
#include <utility.hpp>
#include <ptimer.hpp>
#include <pid_controller.hpp>
#include <cstdlib> //std::strtod
#include <algorithm> //This is necessary to fix a bug that cost me 4h.
#include <Arduino.h>

/*
    As I tracked down, a (standard) string library calls std::min() in some functions (like compare).
    The arduino library device_usb.h defines a min() macro.
    Because <algorithm> wasn't included anywhere in this compilation unit, but device_usb.h is, these calls to std::min() resolve to the device_usb.hpp version.
    This, however, doesn't work, and the compiler throws an error concerning this macro.
    Holy fucking shit.
*/

unsigned long Communicator::timeout_us_ = 20000u; //timeout 20ms
char Communicator::outbuf_[256]; //out buffer
char Communicator::cmd_ = 'X'; //command received
std::vector<char> Communicator::arg_ = std::vector<char>(256, '\0'); //arguments received
unsigned long Communicator::last_transaction_ms_ = 0u; //time of last transaction
unsigned long Communicator::interval_assume_eow_ms_ = 1800000u; //no transaction for 30 minutes means we shut down

bool Communicator::communicate(){
    if(!serialCharAvailable_()){ //no communication
        if(msElapsedSinceLastTransaction_() > interval_assume_eow_ms_){
            //no sign of supervision for 30 minutes, shut down
            Pid_controller::turn_off();
        }
        else{
            //have supervision, turn PID Controller on
            Pid_controller::turn_on();
        }
        return false;
    }
    else{
        startTransactionTimer_();
        Pid_controller::turn_on(); //have supervision, make sure PID Controller is enabled
        cmd_ = getSerialChar_();
        if(isSetCommand_()){
            //read argument
            char latest = cmd_;
            size_t i = 0;
            PTimer::start();
            while(latest != ';' && i < arg_.size()-10 && PTimer::elapsed() < timeout_us_){
                if(serialCharAvailable_()){
                    latest = getSerialChar_();
                    arg_[i++] = latest;
                    if(latest == ';'){
                        arg_[i-1] = '\0';
                    }
                    PTimer::start();
                }
            }
            if(PTimer::elapsed() >= timeout_us_){
                handleTimeout_(); //timeout
                return false;
            }
            else if(i >= arg_.size()-10){
                handleInputOverflow_(); //input overflow
                return false;
            }
            //apply the setting
            else{
                handleSetCommand_();
                return true;
            }
        }
        else if(isGetCommand_()){
            if(cmd_ == 't'){ //getTemperature
                snprintf(   outbuf_,
                            255,
                            "%f,%f,%f,%f,%f;",
                            Temperatures::temperatures[0],
                            Temperatures::temperatures[1],
                            Temperatures::temperatures[2],
                            Temperatures::temperatures[3],
                            Temperatures::temperatures[4]
                            );
                sendBuf_();
                return true;
            }
            else if(cmd_ == 'p'){ //getPidCoeff
                auto kpids = Pid_controller::get_kpid();
                snprintf(   outbuf_,
                            255,
                            "%f,%f,%f;", 
                            kpids[0],
                            kpids[1],
                            kpids[2]
                            );
                sendBuf_();
                return true;
            }
            else if(cmd_ == 'v'){ //getPropIntDer
                auto pids = Pid_controller::get_pid();
                snprintf(   outbuf_,
                            255,
                            "%f,%f,%f;", 
                            pids[0],
                            pids[1],
                            pids[2]
                            );
                sendBuf_();
                return true;
            }
            else if(cmd_ == 'o'){ //getOutput
                snprintf(outbuf_, 255, "%f;", Pid_controller::get_output());
                sendBuf_();
                return true;
            }
            else if(cmd_ == 's'){ //getSetpoint
                snprintf(outbuf_, 255, "%f;", Pid_controller::get_setpoint());
                sendBuf_();
                return true;
            }
            else{ //invalid command
                handleInvalidCommand_();
                return false;
            }
        }
        else if(isNopCommand_()){
            handleNopCommand_();
            return true;
        }
        else{ //invalid command
            handleInvalidCommand_();
            return false;
        }
    }
}

bool Communicator::serialCharAvailable_(){
    return SERCHAN.available() > 0;
}

bool Communicator::isSetCommand_(){
    return  cmd_ == 'P' ||
            cmd_ == 'I' ||
            cmd_ == 'D' ||
            cmd_ == 'N' ||
            cmd_ == 'S' ||
            cmd_ == 'E';
}

bool Communicator::isGetCommand_(){
    return  cmd_ == 't' ||
            cmd_ == 'p' ||
            cmd_ == 'v' ||
            cmd_ == 'o' ||
            cmd_ == 's';
}

bool Communicator::isNopCommand_(){
    return  cmd_ == '?';
}

char Communicator::getSerialChar_(){
    int newChar = SERCHAN.read();
    if(newChar < 0 || newChar >= 128){//invalid char
        return 'X';
    }
    else{
        return char(newChar);
    }
}

void Communicator::sendBuf_(){
    SERCHAN.write(outbuf_);
}

void Communicator::handleTimeout_(){
    snprintf(outbuf_, 255, "!");
    sendBuf_();
}

void Communicator::handleInputOverflow_(){
    snprintf(outbuf_, 255, "!");
    sendBuf_();
}

void Communicator::handleSetCommand_(){
    char* endptr = arg_.data()+250;
    double darg = std::strtod(arg_.data(), &endptr);
    if(arg_.data() == endptr){ //error
        handleInvalidCommand_();
        return;
    }
    else if(cmd_ == 'P'){
        Pid_controller::set_Kp(darg);
        snprintf(outbuf_, 255, ";");
        sendBuf_();
        return;
    }
    else if(cmd_ == 'I'){
        Pid_controller::set_Ki(darg);
        snprintf(outbuf_, 255, ";");
        sendBuf_();
        return;
    }
    else if(cmd_ == 'D'){
        Pid_controller::set_Kd(darg);
        snprintf(outbuf_, 255, ";");
        sendBuf_();
        return;
    }
    else if(cmd_ == 'N'){
        Pid_controller::set_I_reset(darg);
        snprintf(outbuf_, 255, ";");
        sendBuf_();
        return;
    }
    else if(cmd_ == 'S'){
        Pid_controller::set_setpoint(darg);
        snprintf(outbuf_, 255, ";");
        sendBuf_();
        return;
    }
    else if(cmd_ == 'E'){
        Pid_controller::set_I_reset_errsq(darg);
        snprintf(outbuf_, 255, ";");
        sendBuf_();
        return;
    }
}

void Communicator::handleNopCommand_(){
    snprintf(outbuf_, 255, ";");
    sendBuf_();
    return;
}

void Communicator::handleInvalidCommand_(){
    snprintf(outbuf_, 255, "!");
    sendBuf_();
    return;
}

void Communicator::startTransactionTimer_(){
    last_transaction_ms_ = millis();
}

unsigned long Communicator::msElapsedSinceLastTransaction_(){
    return static_cast<unsigned long>(millis() - last_transaction_ms_);
}