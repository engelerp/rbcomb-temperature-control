#ifndef COMMUNICATOR_HPP_INCLUDED
#define COMMUNICATOR_HPP_INCLUDED
#include <vector>
#include <algorithm>

#define SERCHAN SerialUSB

class Communicator{
public:
    static bool communicate();

private:
    /*Function Members*/
    static bool serialCharAvailable_();
    static bool isSetCommand_();
    static bool isGetCommand_();
    static bool isNopCommand_();

    static char getSerialChar_();
    static void sendBuf_();

    static void handleTimeout_();
    static void handleInputOverflow_();
    static void handleSetCommand_();
    static void handleNopCommand_();
    static void handleInvalidCommand_();

    static void startTransactionTimer_();
    static unsigned long msElapsedSinceLastTransaction_();
    

    /*Data Members*/
    static unsigned long timeout_us_;
    static char outbuf_[256];
    static char cmd_;
    static std::vector<char> arg_;

    //Note: The timer only works for timeouts that last shorter than 50 days
    static unsigned long last_transaction_ms_;
    static unsigned long interval_assume_eow_ms_;
};

#endif
