#include <utility.hpp>
#include <ad7124_4.hpp>
#include <ad7124_registers.hpp>
#include <spi_selecta.hpp>

std::array<double,5> Temperatures::temperatures = {0., 0., 0., 0., 0.};

Adc_data readAdc(Ad7124_4* device){
    Adc_data data;
    data.valid = false;
    Spi_selecta::select_device(device);
    if(device->data_available()){
        Spi_selecta::deselect_device();
        Spi_selecta::select_device(device);
        data.channel = device->read_register(REG_Status, 1);
        Spi_selecta::deselect_device();
        Spi_selecta::select_device(device);
        data.data = device->get_data() & 0x00FFFFFF;
        Spi_selecta::deselect_device();
        data.valid = true;
    }
    Spi_selecta::deselect_device();
    return data;
}
