#ifndef UTILITY_HPP_INCLUDED
#define UTILITY_HPP_INCLUDED
#include <ad7124_4.hpp>
#include <array>

struct Adc_data{
    bool valid;
    uint32_t channel;
    uint32_t data;
};

Adc_data readAdc(Ad7124_4* device);

struct Temperatures{
    /*The 0th temperature shall always be the loop, the others the out-of-loops*/
    static std::array<double, 5> temperatures;
};

#endif
