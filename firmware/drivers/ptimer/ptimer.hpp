#ifndef PTIMER_HPP_INCLUDED
#define PTIMER_HPP_INCLUDED
class PTimer{
public:
    static void start();
    //returns the elapsed time in microseconds
    static unsigned long elapsed();
private:
    static unsigned long start_t_;
};
#endif
