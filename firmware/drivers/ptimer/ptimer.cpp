#include <ptimer.hpp>
#include <Arduino.h>

unsigned long PTimer::start_t_ = 0;

void PTimer::start(){
    start_t_ = micros();
}

unsigned long PTimer::elapsed(){
    return static_cast<unsigned long>(micros() - start_t_);
}
