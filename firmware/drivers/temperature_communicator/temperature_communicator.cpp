#include <temperature_communicator.hpp>
#include <cstdio> //snprintf
#include <utility.hpp>
#include <cstdlib> //std::strtod
#include <algorithm> //This is necessary to fix a bug that cost me 4h.
#include <Arduino.h>

char TemperatureCommunicator::outbuf_[256]; //out buffer
char TemperatureCommunicator::cmd_ = 'X'; //command received

bool TemperatureCommunicator::communicate(){
    if(!serialCharAvailable_()){ //no communication
    //    if(msElapsedSinceLastTransaction_() > interval_assume_eow_ms_){
            //no sign of supervision for 30 minutes
    //    }
    //    else{
            //have supervision
    //    }
        return false;
    }
    else{
        cmd_ = getSerialChar_();
        if(cmd_ == 't'){ //getTemperature
            snprintf(   outbuf_,
                        255,
                        "%f,%f,%f,%f,%f;",
                        Temperatures::temperatures[0],
                        Temperatures::temperatures[1],
                        Temperatures::temperatures[2],
                        Temperatures::temperatures[3],
                        Temperatures::temperatures[4]
                        );
            sendBuf_();
            return true;
        }
        else{ //invalid command
            handleInvalidCommand_();
            return false;
        }
    }
}

bool TemperatureCommunicator::serialCharAvailable_(){
    return SERCHAN.available() > 0;
}

char TemperatureCommunicator::getSerialChar_(){
    int newChar = SERCHAN.read();
    if(newChar < 0 || newChar >= 128){//invalid char
        return 'X';
    }
    else{
        return char(newChar);
    }
}

void TemperatureCommunicator::sendBuf_(){
    SERCHAN.write(outbuf_);
}

void TemperatureCommunicator::handleInvalidCommand_(){
    snprintf(outbuf_, 255, "!");
    sendBuf_();
    return;
}