#ifndef TEMPERATURECOMMUNICATOR_HPP_INCLUDED
#define TEMPERATURECOMMUNICATOR_HPP_INCLUDED
#include <algorithm>
#include <vector>

#define SERCHAN SerialUSB

class TemperatureCommunicator{
public:
    static bool communicate();

private:
    /*Function Members*/
    static bool serialCharAvailable_();

    static char getSerialChar_();
    static void sendBuf_();
    
    static void handleInvalidCommand_();

    /*Data Members*/
    static char outbuf_[256];
    static char cmd_;
};

#endif