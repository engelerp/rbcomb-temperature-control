#include <ad7124_4.hpp>
#include <ad7124_registers.hpp>
#include <SPI.h>
#include <algorithm> //std::min

/*Custom literal operators*/
uint8_t operator"" _u8(unsigned long long int n){return uint8_t(n);}
uint16_t operator"" _u16(unsigned long long int n){return uint16_t(n);}
uint32_t operator"" _u32(unsigned long long int n){return uint32_t(n);}

Ad7124_4::Ad7124_4(unsigned nCS, const SPISettings* spisettings): ncs_(nCS), spisettings_(spisettings), datastatus_(false) {}

/*Selection Function Members*/
//Pull nCS low
void Ad7124_4::select_()
{
    digitalWrite(ncs_, LOW);
}

//Pull nCS high
void Ad7124_4::deselect_()
{
    digitalWrite(ncs_, HIGH);
}

/*Communication Function Members*/
//T must be either uint8_t, uint16_t or uint32_t
template<class T>
void Ad7124_4::write_register(uint8_t reg, T data){
    SPIwrite_(data, reg);
    //detect and track writes to ADC_CONTROL_DATA_STATUS bit
    if(reg == REG_ADC_CONTROL){
        datastatus_ = ((data & ADC_CONTROL_DATA_STATUS) != 0);
    }
    //datastatus_ = ((reg == REG_ADC_CONTROL) && ((data & ADC_CONTROL_DATA_STATUS) != 0));
}

uint32_t Ad7124_4::read_register(uint8_t reg, uint8_t num_bytes){
    if(num_bytes == 1){
        SPIread_(buf8_, reg, num_bytes);
        return static_cast<uint8_t>(buf8_);
    }
    else if(num_bytes == 2){
        SPIread_(buf16_, reg, num_bytes);
        return static_cast<uint32_t>(buf16_);
    }
    else{
        SPIread_(buf32_, reg, num_bytes);
        return buf32_;
    }
}

/*Operation Function Members*/
bool Ad7124_4::data_available()
{
    SPIread_(buf8_, REG_Status, 1);
    return !(buf8_>>7);
}

bool Ad7124_4::check_error(){
    SPIread_(buf8_, REG_Status, 1);
    return !((buf8_>>6) & 0x01_u8);
}

uint32_t Ad7124_4::get_data()
{
    SPIread_(buf32_, REG_Data, 3 + datastatus_);
    return buf32_;
}

uint32_t Ad7124_4::get_error(){
    SPIread_(buf32_, REG_Error, 3);
    return buf32_ & 0x00FFFFFF_u32;
}

bool Ad7124_4::data_status(){
    return datastatus_;
}

void Ad7124_4::reset(){
    SPI.beginTransaction(*spisettings_);
    for(unsigned i = 0; i < 8; ++i){
        SPI.transfer(0xFF);
    }
    SPI.endTransaction();
}

/*Private Function Members*/
//T must be either uint8_t, uint16_t or uint32_t
template <class T>
void Ad7124_4::SPIwrite_(T data, uint8_t reg)
{
    //set up write command
    wrbuf_[0] = reg | COMMS_W;
    uint8_t num_bytes = std::min(sizeof(data), 3u);
    for(unsigned i = 0; i < num_bytes; ++i){
        wrbuf_[num_bytes-i] = data & 0xFF;
        data >>= 8;
    }
    SPI.beginTransaction(*spisettings_);
    SPI.transfer(wrbuf_, num_bytes+1);
    SPI.endTransaction();
}

//T must be either uint8_t, uint16_t or uint32_t
//readlen is the number bytes to read (needed for DATA_STATUS support)
template <class T>
void Ad7124_4::SPIread_(T& data, uint8_t reg, uint8_t readlen)
{
    //set up write command
    rdbuf_[0] = reg | COMMS_R;
    SPI.beginTransaction(*spisettings_);
    SPI.transfer(rdbuf_, readlen+1);
    SPI.endTransaction();
    for(unsigned i = 1; i <= readlen; ++i){
        data <<= 8;
        data += rdbuf_[i];
    }
}

unsigned Ad7124_4::get_ncs_(){
    return ncs_;
}

//explicit instantiations

template void Ad7124_4::write_register<uint8_t>(uint8_t reg, uint8_t data);
template void Ad7124_4::write_register<uint16_t>(uint8_t reg, uint16_t data);
template void Ad7124_4::write_register<uint32_t>(uint8_t reg, uint32_t data);

template void Ad7124_4::SPIwrite_<uint8_t>(uint8_t data, uint8_t reg);
template void Ad7124_4::SPIwrite_<uint16_t>(uint16_t data, uint8_t reg);
template void Ad7124_4::SPIwrite_<uint32_t>(uint32_t data, uint8_t reg);
template void Ad7124_4::SPIread_<uint8_t>(uint8_t& data, uint8_t reg, uint8_t readlen);
template void Ad7124_4::SPIread_<uint16_t>(uint16_t& data, uint8_t reg, uint8_t readlen);
template void Ad7124_4::SPIread_<uint32_t>(uint32_t& data, uint8_t reg, uint8_t readlen);