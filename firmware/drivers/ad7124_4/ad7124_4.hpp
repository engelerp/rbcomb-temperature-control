#ifndef AD7124_4_INCLUDED
#define AD7124_4_INCLUDED
#include <ad7124_registers.hpp>
#include <SPI.h>


class Ad7124_4
{
public:
/*Constructors*/
    Ad7124_4(unsigned nCS, const SPISettings* spisettings);
    Ad7124_4(const Ad7124_4&)=delete;
    ~Ad7124_4()=default;

/*Communication Function Members*/
    //T must be either uint8_t, uint16_t or uint32_t
    template<class T>
    void write_register(uint8_t register, T data);

    uint32_t read_register(uint8_t register, uint8_t num_bytes);

/*Operation Function Members*/
    bool data_available();
    bool check_error();
    bool data_status();
    
    uint32_t get_data();
    uint32_t get_error();

    void reset();


/*Friends*/
    friend class Spi_selecta;

private:
/*Private Function Members*/
    //T must be either uint8_t, uint16_t or uint32_t
    template <class T>
    void SPIwrite_(T data, uint8_t reg);

    //T must be either uint8_t, uint16_t or uint32_t
    //readlen is the number bytes to read (needed for DATA_STATUS support)
    template <class T>
    void SPIread_(T& data, uint8_t reg, uint8_t readlen);

/*Selection Function Members*/
    //Pull nCS low
    void select_();

    //Pull nCS high
    void deselect_();

    //Get nCS
    unsigned get_ncs_();


/*Private Data Members*/
    const unsigned ncs_;
    const SPISettings* spisettings_;

    uint8_t regbuf_;
    uint8_t buf8_;
    uint16_t buf16_;
    uint32_t buf32_;

    uint8_t wrbuf_[5] = {0, 0, 0, 0, 0};
    uint8_t rdbuf_[5] = {0, 0, 0, 0, 0};

    bool datastatus_;
};

#endif