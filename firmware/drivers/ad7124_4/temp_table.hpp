#ifndef TEMP_TABLE_INCLUDED
#define TEMP_TABLE_INCLUDED
#include <cstdint>

double data_to_temperature(uint32_t data);

#endif