#ifndef AD7124_REGISTERS_INCLUDED
#define AD7124_REGISTERS_INCLUDED
#include <stdint.h>

/*Custom literal operators*/
uint8_t operator"" _u8(unsigned long long int n);
uint16_t operator"" _u16(unsigned long long int n);
uint32_t operator"" _u32(unsigned long long int n);

/*Communications register, WRITE ONLY*/
//---Address
#define REG_COMMS                                   0x00_u8
//---Write Values
#define COMMS_R                                     0b01000000_u8
#define COMMS_W                                     0b00000000_u8

/*Status register, READ ONLY*/
//---Address
#define REG_Status                                  0x00_u8
//---Read Masks
#define Status_nRDY                                 0b10000000_u8
#define Status_ERROR_FLAG                           0b01000000_u8
#define Status_POR_FLAG                             0b00010000_u8
#define Status_CH_ACTIVE                            0b00001111_u8

/*ADC Control register, READ/WRITE*/
//---Address
#define REG_ADC_CONTROL                             0x01_u8
//---Read Masks
#define ADC_CONTROL_DOUT_nRDY_DEL                   0b0001000000000000_u16
#define ADC_CONTROL_DOUT_CONT_READ                  0b0000100000000000_u16
#define ADC_CONTROL_DATA_STATUS                     0b0000010000000000_u16
#define ADC_CONTROL_nCS_EN                          0b0000001000000000_u16
#define ADC_CONTROL_REF_EN                          0b0000000100000000_u16
#define ADC_CONTROL_POWER_MODE                      0b0000000011000000_u16
#define ADC_CONTROL_Mode                            0b0000000000111100_u16
#define ADC_CONTROL_CLK_SEL                         0b0000000000000011_u16
//---Write Masks
#define ADC_CONTROL_DOUT_nRDY_DEL_1                 0b0001000000000000_u16
#define ADC_CONTROL_DOUT_CONT_READ_1                0b0000100000000000_u16
#define ADC_CONTROL_DATA_STATUS_1                   0b0000010000000000_u16
#define ADC_CONTROL_nCS_EN_1                        0b0000001000000000_u16
#define ADC_CONTROL_REF_EN_1                        0b0000000100000000_u16
#define ADC_CONTROL_POWER_MODE_LOW_POWER            0b0000000000000000_u16
#define ADC_CONTROL_POWER_MODE_MID_POWER            0b0000000001000000_u16
#define ADC_CONTROL_POWER_MODE_FULL_POWER           0b0000000011000000_u16
#define ADC_CONTROL_Mode_CONTINUOUS                 0b0000000000000000_u16
#define ADC_CONTROL_Mode_SINGLE                     0b0000000000000100_u16
#define ADC_CONTROL_Mode_STANDBY                    0b0000000000001000_u16
#define ADC_CONTROL_Mode_POWER_DOWN                 0b0000000000001100_u16
#define ADC_CONTROL_Mode_IDLE                       0b0000000000010000_u16
#define ADC_CONTROL_Mode_INT_OFFSET_CALIB           0b0000000000010100_u16
#define ADC_CONTROL_Mode_INT_GAIN_CALIB             0b0000000000011000_u16
#define ADC_CONTROL_Mode_EXT_OFFSET_CALIB           0b0000000000011100_u16
#define ADC_CONTROL_Mode_EXT_GAIN_CALIB             0b0000000000100000_u16
#define ADC_CONTROL_CLK_SEL_INTERNAL                0b0000000000000000_u16
#define ADC_CONTROL_CLK_SEL_INTERNAL_PIN            0b0000000000000001_u16
#define ADC_CONTROL_CLK_SEL_EXTERNAL                0b0000000000000010_u16
#define ADC_CONTROL_CLK_SEL_EXTERNAL_DIV4           0b0000000000000011_u16

/*Data register, READ ONLY*/
#define REG_Data                                    0x02_u8
//---Read Masks
#define Data_MASK                                   0xFFFFFF_u32

/*IO Control register 1, READ/WRITE*/
#define REG_IO_CONTROL_1                            0x03_u8

/*IO Control register 2, READ/WRITE*/
#define REG_IO_CONTROL_2                            0x04_u8

/*ID register, should read 0x04 (or 0x06 for B-grade), READ ONLY*/
#define REG_ID                                      0x05_u8

/*Error register, READ ONLY*/
#define REG_Error                                   0x06_u8

/*Error Enable register, READ/WRITE*/
#define REG_ERROR_EN                                0x07_u8

/*Master Clock Frequency Monitoring register, READ ONLY*/
#define REG_MCLK_COUNT                              0x08_u8

/*Channel registers, READ/WRITE*/
//---Addresses
#define REG_CHANNEL_0                               0x09_u8
#define REG_CHANNEL_1                               0x0A_u8
#define REG_CHANNEL_2                               0x0B_u8
#define REG_CHANNEL_3                               0x0C_u8
#define REG_CHANNEL_4                               0x0D_u8
#define REG_CHANNEL_5                               0x0E_u8
#define REG_CHANNEL_6                               0x0F_u8
#define REG_CHANNEL_7                               0x10_u8
#define REG_CHANNEL_8                               0x11_u8
#define REG_CHANNEL_9                               0x12_u8
#define REG_CHANNEL_10                              0x13_u8
#define REG_CHANNEL_11                              0x14_u8
#define REG_CHANNEL_12                              0x15_u8
#define REG_CHANNEL_13                              0x16_u8
#define REG_CHANNEL_14                              0x17_u8
#define REG_CHANNEL_15                              0x18_u8
//---Read Masks
#define CHANNEL_Enable                              0b1000000000000000_u16
#define CHANNEL_Setup                               0b0111000000000000_u16
#define CHANNEL_AINP                                0b0000001111100000_u16
#define CHANNEL_AINM                                0b0000000000011111_u16
//---Write Masks
#define CHANNEL_Enable_1                            0b1000000000000000_u16
#define CHANNEL_Setup_0                             0b0000000000000000_u16
#define CHANNEL_Setup_1                             0b0001000000000000_u16
#define CHANNEL_Setup_2                             0b0010000000000000_u16
#define CHANNEL_Setup_3                             0b0011000000000000_u16
#define CHANNEL_Setup_4                             0b0100000000000000_u16
#define CHANNEL_Setup_5                             0b0101000000000000_u16
#define CHANNEL_Setup_6                             0b0110000000000000_u16
#define CHANNEL_Setup_7                             0b0111000000000000_u16
#define CHANNEL_AINP_AIN0                           0b0000000000000000_u16
#define CHANNEL_AINP_AIN1                           0b0000000000100000_u16
#define CHANNEL_AINP_AIN2                           0b0000000001000000_u16
#define CHANNEL_AINP_AIN3                           0b0000000001100000_u16
#define CHANNEL_AINP_AIN4                           0b0000000010000000_u16
#define CHANNEL_AINP_AIN5                           0b0000000010100000_u16
#define CHANNEL_AINP_AIN6                           0b0000000011000000_u16
#define CHANNEL_AINP_AIN7                           0b0000000011100000_u16
#define CHANNEL_AINP_TEMP                           0b0000001000000000_u16
#define CHANNEL_AINP_AVSS                           0b0000001000100000_u16
#define CHANNEL_AINP_REF_INT                        0b0000001001000000_u16
#define CHANNEL_AINP_DGND                           0b0000001001100000_u16
#define CHANNEL_AINM_AIN0                           0b0000000000000000_u16
#define CHANNEL_AINM_AIN1                           0b0000000000000001_u16
#define CHANNEL_AINM_AIN2                           0b0000000000000010_u16
#define CHANNEL_AINM_AIN3                           0b0000000000000011_u16
#define CHANNEL_AINM_AIN4                           0b0000000000000100_u16
#define CHANNEL_AINM_AIN5                           0b0000000000000101_u16
#define CHANNEL_AINM_AIN6                           0b0000000000000110_u16
#define CHANNEL_AINM_AIN7                           0b0000000000000111_u16
#define CHANNEL_AINM_TEMP                           0b0000000000010000_u16
#define CHANNEL_AINM_AVSS                           0b0000000000010001_u16
#define CHANNEL_AINM_REF_INT                        0b0000000000010010_u16
#define CHANNEL_AINM_DGND                           0b0000000000010011_u16

/*Configuration registers, READ/WRITE*/
//---Addresses
#define REG_CONFIG_0                                0x19_u8
#define REG_CONFIG_1                                0x1A_u8
#define REG_CONFIG_2                                0x1B_u8
#define REG_CONFIG_3                                0x1C_u8
#define REG_CONFIG_4                                0x1D_u8
#define REG_CONFIG_5                                0x1E_u8
#define REG_CONFIG_6                                0x1F_u8
#define REG_CONFIG_7                                0x20_u8
//---Read Masks
#define CONFIG_Bipolar                              0b0000100000000000_u16
#define CONFIG_Burnout                              0b0000011000000000_u16
#define CONFIG_REF_BUFP                             0b0000000100000000_u16
#define CONFIG_REF_BUFM                             0b0000000010000000_u16
#define CONFIG_AIN_BUFP                             0b0000000001000000_u16
#define CONFIG_AIN_BUFM                             0b0000000000100000_u16
#define CONFIG_REF_SEL                              0b0000000000011000_u16
#define CONFIG_PGA                                  0b0000000000000111_u16
//---Write Masks
#define CONFIG_Bipolar_1                            0b0000100000000000_u16
#define CONFIG_Burnout_OFF                          0b0000000000000000_u16
#define CONFIG_Burnout_0UA5                         0b0000001000000000_u16
#define CONFIG_Burnout_2UA                          0b0000010000000000_u16
#define CONFIG_Burnout_4UA                          0b0000011000000000_u16
#define CONFIG_REF_BUFP_1                           0b0000000100000000_u16
#define CONFIG_REF_BUFM_1                           0b0000000010000000_u16
#define CONFIG_AIN_BUFP_1                           0b0000000001000000_u16
#define CONFIG_AIN_BUFM_1                           0b0000000000100000_u16
#define CONFIG_REF_SEL_REFIN1                       0b0000000000000000_u16
#define CONFIG_REF_SEL_REFIN2                       0b0000000000001000_u16
#define CONFIG_REF_SEL_INTERNAL                     0b0000000000010000_u16
#define CONFIG_REF_SEL_AVDD                         0b0000000000011000_u16
#define CONFIG_PGA_GAIN1                            0b0000000000000000_u16
#define CONFIG_PGA_GAIN2                            0b0000000000000001_u16
#define CONFIG_PGA_GAIN4                            0b0000000000000010_u16
#define CONFIG_PGA_GAIN8                            0b0000000000000011_u16
#define CONFIG_PGA_GAIN16                           0b0000000000000100_u16
#define CONFIG_PGA_GAIN32                           0b0000000000000101_u16
#define CONFIG_PGA_GAIN64                           0b0000000000000110_u16
#define CONFIG_PGA_GAIN128                          0b0000000000000111_u16

/*Filter registers, READ/WRITE*/
//---Addresses
#define REG_FILTER_0                                0x21_u8
#define REG_FILTER_1                                0x22_u8
#define REG_FILTER_2                                0x23_u8
#define REG_FILTER_3                                0x24_u8
#define REG_FILTER_4                                0x25_u8
#define REG_FILTER_5                                0x26_u8
#define REG_FILTER_6                                0x27_u8
#define REG_FILTER_7                                0x28_u8
//---Read Masks
#define FILTER_Filter                               0b111000000000000000000000_u32
#define FILTER_REJ60                                0b000100000000000000000000_u32
#define FILTER_POST_FILTER                          0b000011100000000000000000_u32
#define FILTER_SINGLE_CYCLE                         0b000000010000000000000000_u32
#define FILTER_FS                                   0b000000000000011111111111_u32
//---Write Masks
#define FILTER_Filter_SINC4                         0b000000000000000000000000_u32
#define FILTER_Filter_SINC3                         0b010000000000000000000000_u32
#define FILTER_Filter_SINC4_FASTSETTLING            0b100000000000000000000000_u32
#define FILTER_Filter_SINC3_FASTSETTLING            0b101000000000000000000000_u32
#define FILTER_Filter_POST_FILTER                   0b111000000000000000000000_u32
#define FILTER_REJ60_1                              0b000100000000000000000000_u32
#define FILTER_POST_FILTER_SPS27                    0b000001000000000000000000_u32
#define FILTER_POST_FILTER_SPS25                    0b000001100000000000000000_u32
#define FILTER_POST_FILTER_SPS20                    0b000010100000000000000000_u32
#define FILTER_POST_FILTER_SPS16                    0b000011000000000000000000_u32
#define FILTER_SINGLE_CYCLE_1                       0b000000010000000000000000_u32
#define FILTER_FS_2047                              0x0007FF_u32
#define FILTER_FS_1920                              0x000780_u32
#define FILTER_FS_1280                              0x000500_u32
#define FILTER_FS_960                               0x0003C0_u32
#define FILTER_FS_640                               0x000280_u32
#define FILTER_FS_480                               0x0001E0_u32
#define FILTER_FS_384                               0x000180_u32
#define FILTER_FS_320                               0x000140_u32
#define FILTER_FS_240                               0x0000F0_u32
#define FILTER_FS_160                               0x0000A0_u32
#define FILTER_FS_120                               0x000078_u32
#define FILTER_FS_80                                0x000050_u32
#define FILTER_FS_60                                0x00003C_u32
#define FILTER_FS_40                                0x000028_u32
#define FILTER_FS_30                                0x00001E_u32
#define FILTER_FS_20                                0x000014_u32
#define FILTER_FS_15                                0x00000F_u32
#define FILTER_FS_10                                0x00000A_u32
#define FILTER_FS_8                                 0x000008_u32
#define FILTER_FS_6                                 0x000006_u32
#define FILTER_FS_4                                 0x000005_u32
#define FILTER_FS_3                                 0x000003_u32
#define FILTER_FS_2                                 0x000002_u32
#define FILTER_FS_1                                 0x000001_u32

/*Offset registers, READ/WRITE*/
#define REG_OFFSET_0                                0x29_u8
#define REG_OFFSET_1                                0x2A_u8
#define REG_OFFSET_2                                0x2B_u8
#define REG_OFFSET_3                                0x2C_u8
#define REG_OFFSET_4                                0x2D_u8
#define REG_OFFSET_5                                0x2E_u8
#define REG_OFFSET_6                                0x2F_u8
#define REG_OFFSET_7                                0x30_u8

/*Gain registers, READ/WRITE*/
#define REG_GAIN_0                                  0x31_u8
#define REG_GAIN_1                                  0x32_u8
#define REG_GAIN_2                                  0x33_u8
#define REG_GAIN_3                                  0x34_u8
#define REG_GAIN_4                                  0x35_u8
#define REG_GAIN_5                                  0x36_u8
#define REG_GAIN_6                                  0x37_u8
#define REG_GAIN_7                                  0x38_u8

#endif