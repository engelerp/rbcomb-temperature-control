#ifndef SPI_SELECTA_INCLUDED
#define SPI_SELECTA_INCLUDED

#include <vector>
#include <ad7124_4.hpp>

struct Spi_selecta{
    Spi_selecta();
    ~Spi_selecta()=default;
    Spi_selecta(const Spi_selecta&)=delete;

    static std::vector<Ad7124_4*> devices;

    static void register_device(Ad7124_4* dev);
    static void select_device(Ad7124_4* dev);
    static void deselect_device();
};

#endif