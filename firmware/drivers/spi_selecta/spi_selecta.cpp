#include <spi_selecta.hpp>

std::vector<Ad7124_4*> Spi_selecta::devices;

void Spi_selecta::register_device(Ad7124_4* dev){
    if(devices.size() == 0){
        //Maximum number of ADCs on the bus supported
        devices.reserve(4);
    }
    devices.push_back(dev);
    digitalWrite(dev->get_ncs_(), HIGH);
    pinMode(dev->get_ncs_(), OUTPUT);
    digitalWrite(dev->get_ncs_(), HIGH);
    dev->deselect_();
}

void Spi_selecta::select_device(Ad7124_4* dev){
    for(auto d: devices){
        d->deselect_();
    }
    dev->select_();
}

void Spi_selecta::deselect_device(){
    for(auto d: devices){
        d->deselect_();
    }
}