#ifndef LTC6992_HPP_INCLUDED
#define LTC6992_HPP_INCLUDED
#include <stdint.h>
#include <Arduino.h>

#define LTC6992_PIN DAC1

struct Ltc6992{
    //first call to a member must be to init()
    static void init();
    //lev is in [0, 1]
    static bool set_level(double lev);

    static double current_level();

    static unsigned current_ulevel();

private:
    static bool initialized_;
    static uint16_t current_level_;
    static uint16_t max_level_;
    static uint16_t min_level_;
};

#endif