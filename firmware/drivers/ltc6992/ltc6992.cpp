#include <ltc6992.hpp>
#include <stdint.h>

bool Ltc6992::initialized_ = false;
uint16_t Ltc6992::current_level_ = 0b0000000000000000;
uint16_t Ltc6992::max_level_ = 0b0000111111111111;
uint16_t Ltc6992::min_level_ = 0b0000000000000000;

void Ltc6992::init(){
    analogWriteResolution(12);
    current_level_ = 0b0000000000000000;
    analogWrite(LTC6992_PIN, current_level_);
    initialized_ = true;
}

bool Ltc6992::set_level(double lev){
    if(!initialized_){
        return false;
    }
    if(lev > 1.0 || lev < 0.0){
        //If input out of bounds, silently turn off
        lev = 0.0;
    }
    uint16_t new_level = static_cast<uint16_t>(static_cast<int>(static_cast<double>(static_cast<int>(max_level_)) * lev));
    if(new_level > max_level_ || new_level < min_level_){
        //If something strange happens, turn off
        current_level_ = min_level_;
    }
    else{
        current_level_ = new_level;
    }
    analogWrite(LTC6992_PIN, current_level_);
    return current_level_ == new_level;
}

double Ltc6992::current_level(){
    return static_cast<double>(current_level_) / static_cast<double>(max_level_);
}

unsigned Ltc6992::current_ulevel(){
    return unsigned(current_level_);
}