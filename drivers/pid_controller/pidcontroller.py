import serial
import time

class PidController:
    def __init__(self, port):
        self.port = port
        try:
            self.ser = serial.Serial(self.port, baudrate=9600, timeout=2)
            print("PID Controller Connected Successfully")
            self.connected = True
        except:
            self.ser = None
            print(f"Failed to open Serial Port {self.port}.")
            self.connected = False
    
    def connect(self, port):
        self.port = port
        try:
            self.ser = serial.Serial(self.port, baudrate=9600, timeout=2)
        except:
            self.ser = None
            print(f"Failed to open Serial Port {self.port}.")
    
    def reconnect(self):
        try:
            self.ser = serial.Serial(self.port, baudrate=9600, timeout=2)
            self.connected = True
        except:
            self.ser = None
            print(f"Failed to open Serial Port {self.port}.")
            self.connected = False


    def disconnect(self):
        self.ser.close()
    
    def _sendCommand(self, command):
        try:
            self.ser.write(command.encode())
        except:
            print(f"Serial port {self.port} is not open.")
            return ""
        time.sleep(0.5)
        text = ""
        while(self.ser.in_waiting):
            text += self.ser.read().decode()
        return text
    
    def getTemperatures(self):
        text = self._sendCommand('t')
        if(text == '!'):
            return '!'
        texts = text.split(',')
        temps = []
        for t in texts:
            if t[-1] == ';':
                temps.append(float(t[:-1]))
            else:
                temps.append(float(t))
        return temps
    
    def getPidCoeff(self):
        text = self._sendCommand('p')
        if(text == '!'):
            return '!'
        texts = text.split(',')
        coeffs = []
        for t in texts:
            if t[-1] == ';':
                coeffs.append(float(t[:-1]))
            else:
                coeffs.append(float(t))
        return coeffs
    
    def getPropIntDer(self):
        text = self._sendCommand('v')
        if(text == '!'):
            return '!'
        texts = text.split(',')
        prinde = []
        for t in texts:
            if t[-1] == ';':
                prinde.append(float(t[:-1]))
            else:
                prinde.append(float(t))
        return prinde

    def getOutput(self):
        text = self._sendCommand('o')
        if(text == '!'):
            return '!'
        return float(text[:-1])

    def getSetpoint(self):
        text = self._sendCommand('s')
        if(text == '!'):
            return '!'
        return float(text[:-1])

    def setP(self, pval):
        text = self._sendCommand(f'P{pval};')
        return text

    def setI(self, ival):
        text = self._sendCommand(f'I{ival};')
        return text

    def setD(self, dval):
        text = self._sendCommand(f'D{dval};')
        return text

    def setIntegral(self, integral):
        text = self._sendCommand(f'N{integral};')
        return text

    def setTemperature(self, temperature):
        text = self._sendCommand(f'S{temperature};')
        return text

    def setIntResErr(self, errThresh):
        text = self._sendCommand(f'E{errThresh};')
        return text
    
    def nop(self):
        text = self._sendCommand(f'?')
        return text
