import requests
import json

class PQWSW2:
    def __init__(self, device_ip):
        self.device_ip = device_ip
        #test if device actually responds
        url = f'http://{self.device_ip}/report'
        try:
            response = requests.get(url)
            print("Smart Switch Connected Successfully")
            self.connected = True
        except:
            print(f"Device at {device_ip} is not available. Are you connected to the PQWSW2 Wlan?")
            self.connected = False
    
    def connect(self, device_ip):
        self.device_ip = device_ip
        #test if device actually responds
        url = f'http://{self.device_ip}/report'
        try:
            response = requests.get(url)
        except:
            print(f"Device at {device_ip} is not available. Are you connected to the PQWSW2 Wlan?")
    
    def getPower(self):
        url = f'http://{self.device_ip}/report'
        try:
            response = requests.get(url)
        except:
            return -1.
        json_response = json.loads(response.text)
        return json_response['power']
    
    def getTemperature(self):
        url = f'http://{self.device_ip}/report'
        try:
            response = requests.get(url)
        except:
            return -1.
        json_response = json.loads(response.text)
        return json_response['temperature']
    
    def getState(self):
        url = f'http://{self.device_ip}/report'
        try:
            response = requests.get(url)
        except:
            return None
        json_response = json.loads(response.text)
        return json_response['relay']
    
    def turnOn(self):
        url = f"http://{self.device_ip}/relay?state=1"
        try:
            response = requests.get(url)
        except:
            print("Communication Error")
    
    def turnOff(self):
        url = f"http://{self.device_ip}/relay?state=0"
        try:
            response = requests.get(url)
        except:
            print("Communication Error")


