import requests
import json

with open('.secrets.json') as secrets_file:
    secrets = json.load(secrets_file)

def get_secrets(setting, secrets=secrets):
    try:
        return secrets[setting]
    except KeyError:
        print("Failed to load credentials.")

class Element:
    def __init__(self):
        self.url = get_secrets("ELEMENT_URL")
    
    def warn(self, message):
        myobj = {"text":message,"username":"Zygote"}
        try:
            requests.post(self.url, json=myobj)
            #print(myobj["text"])
        except:
            print("Failed to send message.")