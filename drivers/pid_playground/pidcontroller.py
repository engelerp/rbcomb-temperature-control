import serial
import rl20001
import pqwsw2

class Pidcontroller:
    def __init__(self, port):
        self.port = port
        try:
            self.ser = serial.Serial(self.port, baudrate=9600, timeout=1)
        except:
            print(f"Pidcontroller: Failed to establish serial connection on port {self.port}")
    
    def attachRL20001(self, rl20001_port):
        self.rl20001 = RL20001(rl20001_port)