import numpy as np

#data time and data_dist are measured, f_drive is best guess for resonance frequency
def frequencyTrackingPE(data_time, data_dist, f_drive):
    #filter the data
    window = 15
    start = window
    end = -window
    data_dist_clean = np.zeros(len(data_dist))
    data_time_clean = np.zeros(len(data_time))
    for i in range(window,len(data_dist)):
        data_dist_clean[i] = data_dist[i] - np.mean(data_dist[i-int(window/2):i+int(window/2)])
        data_time_clean[i] = data_time[i]
    
    #find zeros in the filtered data
    #find reference zeros
    zeros_meas = []
    for k in range(len(data_dist_clean[start+1:end])):
        i = start+1+k
        if(data_dist_clean[i]*data_dist_clean[i-1] < 0.):#have sign change
            #calculate location of zero using linear interpolation
            m = (data_dist_clean[i] - data_dist_clean[i-1])/(data_time_clean[i]-data_time_clean[i-1])
            q = m * data_time_clean[i] - data_dist_clean[i]
            zeros_meas.append(-q/m)
    
    data_calc = np.sin(f_drive*np.pi*2.*data_time_clean[start:end]/1e8)*1000.
    time_calc = data_time_clean[start:end]/1e8
    #find zeros in drive data
    zeros_calc = []
    for k in range(1, len(data_calc)):
        if(data_calc[k]*data_calc[k-1] < 0.):#have sign change
            #calculate location of zero using linear interpolation
            if(time_calc[k] == time_calc[k-1]):
                continue
            m = (data_calc[k] - data_calc[k-1])/(time_calc[k]-time_calc[k-1])
            q = m * time_calc[k] - data_calc[k]
            zeros_calc.append(-q/m)
    
    #calculate change in zero distance
    zeros_dists = []
    zeros_times = []
    for i in range(min(len(zeros_calc), len(zeros_meas))):
        zeros_dists.append(zeros_calc[i] - zeros_meas[i]/1e8)
        zeros_times.append(-(zeros_calc[i]+zeros_meas[i]/1e8)/2.)
    
    #calculate implied frequency error
    m,q = np.polyfit(zeros_times, zeros_dists, 1)
    error = m*f_drive
    better_error = m*(f_drive-error/2.)
    
    #return corrected frequency
    return f_drive - better_error