import pidcontroller
import os
import json
import serial
from influxdb import InfluxDBClient
import time
import pqwsw2

with open('.secrets.json') as secrets_file:
    secrets = json.load(secrets_file)

def get_secret(setting, secrets=secrets):
    try:
        return secrets[setting]
    except KeyError:
        print("Failed to load credentials.")

clientDB=InfluxDBClient(host=get_secret('DB_HOSTNAME'), port=get_secret('DB_PORT'), username=get_secret('DB_USERNAME'), password=get_secret('DB_PASSWORD'), database='cmtqo_metamat', ssl=True, verify_ssl=True)

try:
    controller = pidcontroller.PidController('COM10')
    tries = 0
    while not controller.connected and tries < 8:
        time.sleep(1)
        controller = pidcontroller.PidController('COM10')
        tries += 1
    temps = controller.getTemperatures()
    controller.disconnect()
    measure = [{"measurement":"loop temperature", "fields":{"°C":temps[0]}, "tags":{"room":"hpp-p24", "setup": "RBComb"}}]
    clientDB.write_points(measure)
    measure = [{"measurement":"ool1 temperature", "fields":{"°C":temps[1]}, "tags":{"room":"hpp-p24", "setup": "RBComb"}}]
    clientDB.write_points(measure)
    measure = [{"measurement":"ool2 temperature", "fields":{"°C":temps[2]}, "tags":{"room":"hpp-p24", "setup": "RBComb"}}]
    clientDB.write_points(measure)
    measure = [{"measurement":"ool3 temperature", "fields":{"°C":temps[3]}, "tags":{"room":"hpp-p24", "setup": "RBComb"}}]
    clientDB.write_points(measure)
    measure = [{"measurement":"ool4 temperature", "fields":{"°C":temps[4]}, "tags":{"room":"hpp-p24", "setup": "RBComb"}}]
    clientDB.write_points(measure)
    #logging for supervisor
    now = time.time()
    logstring = f"{now},{temps[0]},{temps[1]},{temps[2]},{temps[3]},{temps[4]}"
    with open("templog.dat",'w') as logfile:
        logfile.write(logstring)

except:
    pass

try:
    import sys
    sys.path.append('C:\\Users\\Pascal\\pythonlibs')
    import utilities.environment
    pressure = utilities.environment.pressure('COM7')
    p = pressure.getPressure()
    measure = [{"measurement":"pressure", "fields":{"mBar":p},'tags':{'room':'hpp-p24', 'setup': "RBComb"}}]
    clientDB.write_points(measure)
except Exception as e:
    print(f"Caught an exception: {e}")
    pass

try:
    import sys
    sys.path.append('C:\\Users\\Pascal\\pythonlibs')
    import utilities.environment
    temperature = utilities.environment.temperature()
    p = temperature.getTemperature()
    measure = [{"measurement":"temperatur", "fields":{"°C":p},'tags':{'room':'hpp-p24', 'setup': "RBComb"}}]
    clientDB.write_points(measure)
except Exception as e:
    print(f"Caught an exception: {e}")
    pass



def getPowerSupplyTemperature():
    try:
        temperatureSerial = serial.Serial('COM15', 9600, timeout=0.2)
        temperatureSerial.write(b't')
        temperature = float(temperatureSerial.read(100).decode().split(',')[0])
        return temperature
    except:
        return 0.

try:
    temperaturePSU = getPowerSupplyTemperature()
    measure = [{"measurement":"temperaturePSU", "fields":{"°C":temperaturePSU},'tags':{'room':'hpp-p24', 'setup': "RBComb"}}]
    clientDB.write_points(measure)
except Exception as e:
    pass

try:
    mySwitch = pqwsw2.PQWSW2('192.168.254.1')
    power = mySwitch.getPower()
    measure = [{"measurement":"powerHeating", "fields":{"W":power},'tags':{'room':'hpp-p24', 'setup': "RBComb"}}]
    clientDB.write_points(measure)
except:
    pass

clientDB.close()