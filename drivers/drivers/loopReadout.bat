setlocal enabledelayedexpansion
@echo off
set "filename=tempfile"

:loop
python readout.py elo > tempfile
cls
set "content="
for /f "delims=" %%a in ('type "%filename%"') do (
    set "line=%%a"
    echo !line!
)
:: del tempfile
timeout /T 12 /nobreak > nul
goto loop
endlocal