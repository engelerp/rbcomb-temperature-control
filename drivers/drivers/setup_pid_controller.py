#this is the configuration that was found to be stable
import pidcontroller

controller = pidcontroller.PidController('COM10')

responses = []

responses.append(controller._sendCommand('P2.254;'))
responses.append(controller._sendCommand('I0.0014;'))
responses.append(controller._sendCommand('D100.348;'))
responses.append(controller._sendCommand('N349.35;'))
responses.append(controller._sendCommand('E0.5;'))
responses.append(controller._sendCommand('S31.0;'))

print(responses)