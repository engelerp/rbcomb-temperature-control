import time
import element
import pqwsw2
import rl20001

warn_thresh = [31.5, 26.0, 33.0, 28.0, 40.0]
shutdown_thresh = [32.0, 30.0, 37.0, 30.0, 46.5]


mySwitch = pqwsw2.PQWSW2('192.168.254.1')
relay = rl20001.RL20001('COM4')

tries = 0
while not mySwitch.connected and tries < 5:
    time.sleep(1)
    mySwitch = pqwsw2.PQWSW2('192.168.254.1')
    tries += 1

tries = 0
while not relay.connected and tries < 5:
    time.sleep(1)
    relay = rl20001.RL20001('COM4')
    tries += 1


myElementConnection = element.Element()

def handler():
    #check if switch is available
    safety_compromised = False
    if (not mySwitch.connected):
        #print("Switch disconnected")
        myElementConnection.warn(f"Can't connect to PQWSW2")
        safety_compromised = True
    
    if(not relay.connected):
        #print("Relay disconnected")
        myElementConnection.warn(f"Can't connect to RL20001.")
        safety_compromised = True
    
    if(not mySwitch.connected and not relay.connected):
        #print("Switch and Relay disconnected")
        myElementConnection.warn(f"All safety devices lost, automatic intervention disabled.")
        safety_compromised = True

    for repetitions_i in range(10):
        try:
            with open('templog.dat') as logfile:
                line = logfile.readline()
            logdatas = line.split(',')
            timestamp_log = float(logdatas[0])
            temps = [float(x) for x in logdatas[1:]]
            print(f"Temperatures: {temps} °C")
            #check for overtemperatures
            overtemp_detected = False
            for i,t in enumerate(temps):
                if t >= warn_thresh[i]:
                    #print("Over Thresh")
                    myElementConnection.warn(f"Temperature {i} over warning threshold ({warn_thresh[i]:.4}°C) detected: {t:.5}°C")
                if t >= shutdown_thresh[i]:
                    #print("Over Shutdown")
                    myElementConnection.warn(f"Temperature {i} over shutdown threshold ({shutdown_thresh[i]:.4}°C) detected: {t:.5}°C. Shutting down.")
                    overtemp_detected = True
                    if mySwitch.connected:
                        mySwitch.turnOff()
                    if relay.connected:
                        relay.turnOff()
            #check for old timestamp
            old_timestamp_detected = False
            now = time.time()
            if now-timestamp_log > 60:
                #no log for 1 minute
                myElementConnection.warn(f"Latest temperature measurement is older than 1 minute ({now-timestamp_log} seconds). Will shut down at 900 seconds.")
            if now-timestamp_log > 600:
                #no log for 1 minute
                myElementConnection.warn(f"Latest temperature measurement is older than 15 minutes ({now-timestamp_log} seconds). Shutting down")
                old_timestamp_detected = True
                if mySwitch.connected:
                    mySwitch.turnOff()
                if relay.connected:
                    relay.turnOff()
            if not overtemp_detected and not old_timestamp_detected:
                #print("Have No Overtemp")
                if mySwitch.connected:
                    mySwitch.turnOn()
                if relay.connected:
                    relay.turnOn()


            if safety_compromised:
                #print("Safety Compromised")
                myElementConnection.warn(f"Safety compromised. Temperatures: {temps} °C")
            if repetitions_i > 0:
                myElementConnection.warn(f"Logfile access recovered.")
            
            return
        except Exception as expt:
            myElementConnection.warn(f"Supervisor Error: {str(expt)}: try {repetitions_i+1}/10")
            time.sleep(3.14159265)
    
    myElementConnection.warn(f"Temperature logfile inaccessible. Shutting down.")
    mySwitch.turnOff()
    relay.turnOff()



handler()