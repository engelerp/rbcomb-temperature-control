import time
import element
import pqwsw2
import rl20001
import pidcontroller
import sys

def readoutController(controller):
    temps = controller.getTemperatures()
    coeffs = controller.getPidCoeff()
    pid = controller.getPropIntDer()
    output = controller.getOutput()
    setpoint = controller.getSetpoint()
    returnString = ""

    returnString += f"PID Controller:\n"
    returnString += f"\tCurrent Temperatures: \n"
    returnString += f"\t\tloop: {temps[0]:.6f} °C\n"
    returnString += f"\t\tool1: {temps[1]:.6f} °C\n"
    returnString += f"\t\tool2: {temps[2]:.6f} °C\n"
    returnString += f"\t\tool3: {temps[3]:.6f} °C\n"
    returnString += f"\t\tool4: {temps[4]:.6f} °C\n"
    returnString += f"\tPID Coefficients:\n"
    returnString += f"\t\tKp: {coeffs[0]:>10.6f}\n"
    returnString += f"\t\tKi: {coeffs[1]:>10.6f}\n"
    returnString += f"\t\tKd: {coeffs[2]:>10.6f}\n"
    returnString += f"\tCurrent PID Values:\n"
    returnString += f"\t\tP: {pid[0]:>10.6f}\n"
    returnString += f"\t\tI: {pid[1]:>10.6f}\n"
    returnString += f"\t\tD: {pid[2]:>10.6f}\n"
    returnString += f"\tCurrent control output:\n"
    returnString += f"\t\t{output:.6f}\n"
    returnString += f"\tCurrent Setpoint:\n"
    returnString += f"\t\t{setpoint:.6f}\n"

    return returnString

def readoutSwitch(switch):
    power = switch.getPower()
    temperature = switch.getTemperature()
    state = switch.getState()
    returnString = ""

    returnString += f"PQWSW2 WIFI Switch:\n"
    returnString += f"\tState:\n"
    returnString += f"\t\t{state}\n"
    returnString += f"\tCurrent Temperature:\n"
    returnString += f"\t\t{temperature:.6f} °C\n"
    returnString += f"\tCurrent Powerdraw:\n"
    returnString += f"\t\t{power} W\n"

    return returnString

def readoutRelays(relays):
    returnString = ""

    returnString += f"RL20001 Numato Relay Board:\n"
    returnString += f"\tState:\n"
    if relays.getState():
        returnString += f"\t\tTrue\n"
    else:
        returnString += f"\t\tFalse\n"

    return returnString

controllerString = ""
switchString = ""
relayString = ""

connected = True
try:
    controller = pidcontroller.PidController('COM10')
    controllerString = readoutController(controller)
    controller.disconnect()
except:
    connected = False

if not connected:
    exit()

connected = True
try:
    mySwitch = pqwsw2.PQWSW2('192.168.254.1')
    switchString = readoutSwitch(mySwitch)
except:
    connected = False

if not connected:
    exit()

connected = True
try:
    relay = rl20001.RL20001('COM4')
    relayString = readoutRelays(relay)
except:
    connected = False

if not connected:
    exit()



elementPrintString = controllerString + switchString + relayString
if len(sys.argv) == 1:
    myElementConnection = element.Element()
    myElementConnection.warn("Manual Readout Triggered")
    split = elementPrintString.split("\n")
    for i in range(len(split)):
        s = split[i]
        s = "> " + s
        srepped = s.replace("\t", "> ")
        split[i] = srepped

    for s in split:
        myElementConnection.warn(s)
        time.sleep(0.3)
print(elementPrintString)