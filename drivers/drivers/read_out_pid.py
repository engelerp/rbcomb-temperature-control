import pidcontroller

controller = pidcontroller.PidController('COM5')
responses = []
requests = ['t', 'p', 'v', 'o', 's']

for r in requests:
    responses.append(controller._sendCommand(r))

for i in range(len(requests)):
    print(f"{requests[i]} -> {responses[i]}")